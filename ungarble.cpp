#define UInt32  macUIn32
#include <CoreFoundation/CoreFoundation.h>
#undef UInt32

#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>

#include <string>
#include <vector>

using std::string;
using std::vector;

vector<UInt16> MultiByteToUnicodeString(const char* path)
{
    vector<UInt16> result;
    
    CFStringRef cfpath = CFStringCreateWithCString(NULL, path, kCFStringEncodingUTF8);
    
    if (cfpath)
    {
       CFMutableStringRef cfpath2 = CFStringCreateMutableCopy(NULL, 0, cfpath);
       CFRelease(cfpath);
       CFStringNormalize(cfpath2, kCFStringNormalizationFormC);
    
       size_t n = CFStringGetLength(cfpath2);
       for (size_t i = 0; i < n; i++)
       {
         result.push_back(CFStringGetCharacterAtIndex(cfpath2, i));
       }

       CFRelease(cfpath2);
       return result;
    }
    fprintf(stderr, "MultiByteToUnicodeString> cfpath is null\n");
    
    for (size_t i = 0; i < strlen(path); i++)
        result.push_back(wchar_t(path[i] & 255));
    return result;
}

vector<UInt8> GBKToUTF8(const char* path)
{
    vector<UInt8> result;
    
    CFStringRef cfpath = CFStringCreateWithCString(NULL, path, kCFStringEncodingGB_18030_2000);
    
    if (cfpath)
    {
        CFMutableStringRef cfpath2 = CFStringCreateMutableCopy(NULL, 0, cfpath);
        CFRelease(cfpath);
        CFStringNormalize(cfpath2, kCFStringNormalizationFormC);
        
        // size_t n = CFStringGetLength(cfpath2);
        // for (size_t i = 0; i < n; i++)
        // {
        //   printf("%x ", CFStringGetCharacterAtIndex(cfpath2, i));
        // }
        // printf("\n\n");
        
        CFRange rangeToProcess = CFRangeMake(0, CFStringGetLength(cfpath2));
        while (rangeToProcess.length > 0)
        {
            UInt8 localBuffer[1000];
            CFIndex usedBufferLength;
            CFIndex numChars = CFStringGetBytes(cfpath2, rangeToProcess, kCFStringEncodingUTF8, 0, false, localBuffer, 1000, &usedBufferLength);
            // printf("numChars=%lu\n", numChars);
            // printf("usedBufferLength=%lu\n", usedBufferLength);
            if (numChars == 0) break;
            rangeToProcess.location += numChars;
            rangeToProcess.length -= numChars;
            for (int i = 0; i < usedBufferLength; i++)
            {
                result.push_back(localBuffer[i]);
            }
        }
        
        CFRelease(cfpath2);
        return result;
    }
    fprintf(stderr, "GBK2UTF8> cfpath is null\n");
    
    for (size_t i = 0; i < strlen(path); i++)
        result.push_back(UInt8(path[i] & 255));
    return result;
}

template<typename T>
void printVector(const vector<T>& v)
{
    for (size_t i = 0; i < v.size(); i++)
    {
        printf("%x ", v[i]);
    }
    printf("\nsize=%lu\n", v.size());
}

template<typename T>
UInt8* fromVector(const vector<T>& v)
{
    UInt8* s = new UInt8[v.size() + 1];
    for (size_t i = 0; i < v.size(); i++)
    {
        s[i] = v[i] & 255;
    }
    s[v.size()] = 0;
    return s;
}

class Ungarbler
{
private:
    bool interactive, recursive, notest;

    void rename(const char* from, const char* to) const
    {
        int status = ::rename(from, to);
        if (status != 0)
        {
            fprintf(stderr, "rename error, errno = %d\n", errno);
            exit(1);
        }
    }

public:
    Ungarbler(bool i, bool r, bool notest)
        : interactive(i), recursive(r), notest(notest)
    {
    }
    
    void ungarble(const string &file) const
    {
        struct stat st_buf;
        int status = stat(file.c_str(), &st_buf);
        if (status != 0)
        {
            fprintf(stderr, "stat error, errno = %d\n", errno);
            exit(1);
        }
        
        if (S_ISDIR(st_buf.st_mode) && recursive)
        {
            int status = chdir(file.c_str());
            if (status != 0)
            {
                fprintf(stderr, "chdir error, errno = %d\n", errno);
                exit(1);
            }
            
            DIR* dirp = opendir(".");
            if (dirp)
            {
                dirent* dp = NULL;
                while ((dp = readdir(dirp)))
                {
                    if (string(dp->d_name) == ".") continue;
                    if (string(dp->d_name) == "..") continue;
                    ungarble(dp->d_name);
                }
                closedir(dirp);
            }
            
            chdir("..");
        }
        
        const char *s = file.c_str();
        // s is the garbled file name
        vector<UInt16> r = MultiByteToUnicodeString(s);
        UInt8* s1 = fromVector(r);
        // s1 is encoded in GBK
        vector<UInt8> r1 = GBKToUTF8((const char *)s1);
        UInt8* s2 = fromVector(r1);
        // s2 is encoded in UTF8
        
        if (notest)
        {
            if (interactive)
            {
                while (true)
                {
                    printf("Rename %s ==> %s ? (y/n) ", s, s2);
                    fflush(stdout);
                    int c = getchar();
                    while (c == '\n' || c == ' ' || c == '\t' || c == '\r')
                    {
                        c = getchar();
                    }
                    if (c == 'y' || c == 'Y')
                    {
                        rename(s, (const char *)s2);
                        break;
                    }
                    if (c == 'n' || c == 'N')
                    {
                        break;
                    }
                    fprintf(stderr, "Illegal option: %c.  Try again.\n", c);
                }
            }
            else
            {
                printf("Rename %s ==> %s\n", s, s2);
                rename(s, (const char *)s2);
            }
        }
        else
        {
            printf("Will rename %s ==> %s\n", s, s2);
        }
        
        delete[] s1;
        delete[] s2;
    }
};

void printHelp()
{
    printf("%s\n",
        "NAME\n"
        "       ungarble - fix garbled file names in Mac OS X\n"
        "\n"
        "SYNOPSIS\n"
        "       ungarble [options] FILE(S) ... DIRECTORY(S)\n"
        "\n"
        "OPTIONS\n"
        "       -i  interactive mode (ask y/n for each action)\n"
        "\n"
        "       -r  recursively go through directories\n"
        "\n"
        "       --notest\n"
        "           Needed to actually rename the files.\n"
        "           By default ungarble will just print what it wants to do.\n"
        "\n"
        "       --help\n"
        "           print a short summary of available options\n"
        "\n"
        "AUTHOR\n"
        "       Xiao Jia\n"
        "\n"
        "       Send mail to me [at] xiao-jia.com for bug reports and suggestions.\n"
    );
}

int main(int argc, char* argv[])
{
    bool interactive = false;
    bool recursive = false;
    bool notest = false;
    vector<string> files;
    
    for (int i = 1; i < argc; i++)
    {
        string arg(argv[i]);
        if (arg == "-i")
        {
            interactive = true;
        }
        else if (arg == "-r")
        {
            recursive = true;
        }
        else if (arg == "--notest")
        {
            notest = true;
        }
        else if (arg == "--help")
        {
            printHelp();
            return 0;
        }
        else
        {
            files.push_back(arg);
        }
    }
    
    if (files.size() == 0)
    {
        printHelp();
        return 0;
    }
    
    Ungarbler ungarbler(interactive, recursive, notest);
    for (size_t i = 0; i < files.size(); i++)
    {
        ungarbler.ungarble(files[i]);
    }
    return 0;
}
